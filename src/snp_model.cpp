#include "snp_model.hpp"

#include "base_types.hpp"

#include <cmath>

using namespace std;


double calculate_epsilon( char base_quality )
{
    return pow( 10 , -( base_quality / 10.0 ) );
}

// calculate Pr( D_j | B ), i.e.
// see paper for details
double pr_dj_given_b( base dj , strand st , base b , double alpha , double beta, double gamma , double epsilon )
{
    if( b == base::A() || b == base::T() )
    {
        if( dj == b )
            return 1 - epsilon;
        else
            return epsilon / 3;
    }
    else if( b == base::C() )
    {
        if( dj == base::C() )
        {
            if( st == strand::PLUS )
                return ( 1 - epsilon ) * ( beta * ( 1 - gamma ) + ( 1 - beta ) * alpha);
            else // strand == strand::MINUS
                return 1 - epsilon;
        }
        else if( dj == base::T() && st == strand::PLUS )
            return epsilon / 3 + ( 1 - epsilon ) * ( beta * gamma + ( 1 - beta ) * ( 1 - alpha ) );
        else
            return epsilon / 3;
    }
    else // b == base::G()
    {
        if( dj == base::G() )
        {
            if( st == strand::PLUS )
                return 1 - epsilon;
            else // strand == strand::MINUS
                return ( 1 - epsilon ) * ( beta * ( 1 - gamma ) + ( 1 - beta ) * alpha );
        }
        else if( dj == base::A() && st == strand::MINUS )
            return epsilon / 3 + ( 1 - epsilon ) * ( beta * gamma + ( 1 - beta ) * ( 1 - alpha ) );
        else
            return epsilon / 3;
    }
}

