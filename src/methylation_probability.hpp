#ifndef METHYLATION_PROBABILITY_HPP_
#define METHYLATION_PROBABILITY_HPP_

#include "base_types.hpp"

#include <array>

double get_beta( const std::array< optional_base , 5 >& context );

#endif

