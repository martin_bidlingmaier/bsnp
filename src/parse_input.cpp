#include "parse_input.hpp"

#include "find_snps.hpp"

#include <sstream>
#include <string>
#include <set>
#include <limits>

using namespace std;


bool is_option( string word ) 
{
    return !word.empty() && word[ 0 ] == '-';
}

bool is_known_option( string word )
{
    static set< string > known_options = { "-q" , "-Q" , "-a" , "-g" , "-c" , "-s" , "-l" , "-d" , "-b" };
    return known_options.find( word ) != known_options.end();
}
bool is_float_option( string word )
{
    static set< string > float_options = { "-a" , "-g" , "-c" };
    return float_options.find( word ) != float_options.end();
}
bool is_uint_option( string word )
{
    static set< string > uint_options = { "-q" , "-Q" , "-s" };
    return uint_options.find( word ) != uint_options.end();
}

input parse_input( int argc , char** argv )
{
    input result;

    // default values for options:
    result.config.alpha = 0.01;
    result.config.gamma = 0.0;
    result.config.minimum_confidence = 0.95;
    result.config.minimum_mapping_quality = 17;
    result.config.minimum_base_quality = 17;
    result.config.maximum_sample_size = numeric_limits< size_t >::max();
    result.config.aligner = ALIGNER::BSMAP;

    int i = 1;
    for( ; i != argc ; ++i )
    {
        string current_word = argv[ i ];
        if( !is_option( current_word ) )
            break;
        else

        {
            if( !is_known_option( current_word ) )
                throw runtime_error( "unknown option: " + current_word );
            if( i + 1 == argc )
                throw runtime_error( "missing argument to " + current_word );
            ++i;
            string argument = argv[ i ];
            stringstream argument_stream( argument );
            if( is_float_option( current_word ) )
            {
                double argument;
                argument_stream >> argument;
                if( !argument_stream )
                    throw runtime_error( "the option " + current_word + " expects a floating point argument" );
                if( current_word == "-a" )
                    result.config.alpha = argument;
                else if( current_word == "-g" )
                    result.config.gamma = argument;
                else if( current_word == "-c" )
                    result.config.minimum_confidence = argument;
                else
                    assert( false );
            }
            else if( is_uint_option( current_word ) )
            {
                size_t argument;
                argument_stream >> argument;
                if( !argument_stream )
                    throw runtime_error( "the option " + current_word + " expects an unsigned integer argument" );

                if( current_word == "-q" )
                    result.config.minimum_base_quality = argument;
                else if( current_word == "-Q" )
                    result.config.minimum_mapping_quality = argument;
                else if( current_word == "-s" )
                    result.config.maximum_sample_size = argument;
                else
                    assert( false );
            }
            else if( current_word == "-l" )
            {
                if( argument == "BSMAP" )
                    result.config.aligner = ALIGNER::BSMAP;
                else if( argument == "BISMARK" )
                    result.config.aligner = ALIGNER::BISMARK;
                else
                    throw runtime_error( "aligner has to be either \"BMSAP\" or \"BISMARK\"" );
            }
            else if( current_word == "-d" )
                result.dbsnp_path = unique_ptr< string >( new string( argument ) );
            else if( current_word == "-b" )
                result.bed_path = unique_ptr< string >( new string( argument ) );
        }
    }

    if( argc - i != 3 )
        throw runtime_error( "invalid argument number" );
    char** mandatory_arguments = argv + i;
    result.bam_path = mandatory_arguments[ 0 ];
    result.fasta_path = mandatory_arguments[ 1 ];
    result.vcf_path = mandatory_arguments[ 2 ];

    return result;
}
