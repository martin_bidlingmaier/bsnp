#ifndef CALL_SNPS_HPP_
#define CALL_SNPS_HPP_

#include <istream>

#include <api/BamReader.h>

#include "bed_utils.hpp"
#include "fasta_file.hpp"
#include "find_snps.hpp"
#include "db_snp.hpp"

void call_snps( std::ostream& output_vcf_file , const snp_config& config , BamTools::BamReader& bam_reader , indexed_fasta_file& fasta_file , std::vector< bed_region > regions , const db_snp& known_snps = db_snp() );

void call_snps( std::ostream& output_vcf_file , const snp_config& config , BamTools::BamReader& bam_reader , indexed_fasta_file& fasta_file , const db_snp& known_snps = db_snp() );
#endif

