#include "snp.hpp"

#include "snp_model.hpp"

#include <utility>
#include <iomanip>

using namespace std;

snp::snp( string chrom_name , size_t pos , optional_base ref_base , const snp_test_result& test_result )
{
    chromosome_name = move( chrom_name );
    position = pos;
    reference_base = ref_base;
    
    id = 0;
    
    no_variant_probability = test_result.no_variant_probability;
    error_probability = test_result.error_probability;

    sample_depth = test_result.sample_depth;

    mapping_quality = test_result.mapping_quality;
    
    genotype g = test_result.most_likely_genotype;
    
    first_base.b = g.a();
    second_base.b = g.b();
    if( second_base.b == ref_base )
        swap( first_base.b , second_base.b );

    first_base.base_specific_depth = test_result.base_specific_depths[ first_base.b ];
    // this may be wrong at the moment as a 'T' in the read may actually be a 'C' and so on
    
    first_base.base_frequency = ( (double)first_base.base_specific_depth ) / sample_depth;

    if( is_homozygous( g ) )
    {
        first_base.allele_count = 2;
        
        has_second_base = false;
    }
    else
    {
        first_base.allele_count = 1;
        
        has_second_base = true;
        second_base.allele_count = 1;
        // second_base.b was already set
        second_base.base_specific_depth = test_result.base_specific_depths[ second_base.b ]; // inaccurate, see above for first_base
         
        second_base.base_frequency = ( (double)second_base.base_specific_depth ) / sample_depth;
    }
}
bool snp::first_base_is_ref() const
{
    return first_base.b == reference_base;
}
ostream& operator<<( ostream& os , const snp& snp )
{
    const snp::base_specific_data& first_base = snp.first_base;
    const snp::base_specific_data& second_base = snp.second_base;
    
    // to digits after period for every floating point
    os << setprecision( 2 ) << fixed;

    // chromosome number
    os << snp.chromosome_name << "\t";
    // 1 based position within chromosome
    os << snp.position + 1 << "\t";
    
    // id for SNP
    if( snp.id )
        os << snp.id;
    else
        os << ".";
    os << "\t";
    
    // reference base
    os << to_char( snp.reference_base ) << "\t";
    
    // alternative bases
    if( !snp.first_base_is_ref() )
    {
        os << to_char( first_base.b );
        if( snp.has_second_base )
        {
            bool second_base_is_ref = snp.reference_base == second_base.b;
            assert( first_base.b != second_base.b && !second_base_is_ref );
            os << "," << to_char( second_base.b );
        }
    }
    else
    {
        bool second_base_is_ref = snp.reference_base == second_base.b;
        assert( snp.has_second_base && !second_base_is_ref );
        os << to_char( second_base.b );
    }
    os << "\t";

    // call quality
    if( snp.error_probability < 1e-100 )
        os << 1000;
    else
        os << -10 * log10( snp.error_probability );
    os << "\t";
    
    // filter - an snp is only serialized if it passed all filters, so always PASS
    os << "PASS" << "\t";

    
    // INFO fields
    // allele count
    os << "AC=";
    if( !snp.first_base_is_ref() )
    {
        os << first_base.allele_count;
        if( snp.has_second_base )
            os << "," << second_base.allele_count;
    }
    else
        os << second_base.allele_count;
    os << ";";

    // allele frequency
    os << "AF=";
    if( !snp.first_base_is_ref() )
    {
        os << first_base.base_frequency;
        if( snp.has_second_base )
            os << "," << second_base.base_frequency;
    }
    else
        os << second_base.base_frequency;
    os << ";";

    // ploidy of genome, this is for this program always 2
    os << "AN=2;";

    // dbSNP membership
    if( snp.id != 0 )
        os << "DB;";

    // sample depth
    os << "DP=" << snp.sample_depth << ";";
    
    // mapping quality
    os << "MQ=" << snp.mapping_quality; // no semicolon, this is the last info field

    os << "\t"; // INFO end


    // genotype fields
    // FORMAT field
    //os << "GT:AD:DP:GQ:PL" << "\t";
    os << "GT:AD:DP:GQ" << "\t";

    // genotype
    if( !snp.first_base_is_ref() )
    {
        os << "1/";
        if( snp.has_second_base )
            os << "2";
        else
            os << "1";
    }
    else
        os << "0/1";
    os << ":";
    
    // base specific depths
    if( !snp.first_base_is_ref() )
        os << "0,";
    os << first_base.base_specific_depth;
    if( snp.has_second_base )
        os << "," << second_base.base_specific_depth;
    os << ":";

    // sample depth again
    os << snp.sample_depth << ":";

    // probability of call being wrong given the site is variant
    // phred scaled
    double conditional_error_probability = ( snp.error_probability - snp.no_variant_probability ) / ( 1 - snp.no_variant_probability );
    if( conditional_error_probability < 1e-100 )
        os << 1000;
    else
        os << -10 * log10( conditional_error_probability );
    
    //os << ":";

    // TODO: what exactly is 'PL'?
    //os << ".";

    return os;
}

