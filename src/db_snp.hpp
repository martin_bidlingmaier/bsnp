#ifndef DB_SNP_HPP_
#define DB_SNP_HPP_

#include "base_types.hpp"

#include <istream>
#include <map>
#include <vector>
#include <string>
#include <array>

struct db_snp
{
    struct entry
    {
        size_t id_start_index;
        uint32_t position;
        optional_base reference_base;
        std::array< optional_base , 3 > alternatives;
    };
 
    std::map< std::string , std::vector< entry > > entries;
    std::vector< char > id_strings_container;
};

db_snp load_db_snp( std::istream& is );


#endif

