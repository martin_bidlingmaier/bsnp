#include "methylation_probability.hpp"

using namespace std;

bool matches_context( const array< optional_base , 5 >& context , const array< optional_base , 5 >& defined_context )
{
    for( int i = 0 ; i != 5 ; ++i )
    {
        if( is_base( defined_context[ i ] ) && context[ i ] != defined_context[ i ] )
            return false;
    }
    return true;
}

double get_beta( const array< optional_base , 5 >& context )
{
    struct methylation_context
    {
        array< optional_base , 5 > context;
        double beta;
    };
    //static const optional_base A = optional_base::A();
    static const optional_base C = optional_base::C();
    static const optional_base G = optional_base::G();
    //static const optional_base T = optional_base::T();
    static const optional_base N = optional_base::N();

    static const array< methylation_context , 4 > DEFINED_CONTEXTS =
    { {
        { array< optional_base , 5 >{ { N , N , C , G , N } } , 0.75 },
        { array< optional_base , 5 >{ { N , N , C , N , G } } , 0.01 },
        { array< optional_base , 5 >{ { N , C , G , N , N } } , 0.75 },
        { array< optional_base , 5 >{ { C , N , G , N , N } } , 0.01 }
    } };

    for( const methylation_context& defined_context : DEFINED_CONTEXTS )
    {
        if( matches_context( context , defined_context.context ) )
            return defined_context.beta;
    }

    return 0.01;
}
                 
