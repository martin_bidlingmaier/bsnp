#include "bed_utils.hpp"

#include <stdexcept>
#include <sstream>

using namespace std;

vector< bed_region > load_bed( istream& bed_stream )
{
    vector< bed_region > regions;
    
    size_t line_number = 1;
    string line;
    while( getline( bed_stream , line ) )
    {
        istringstream line_stream( line );
        
        bed_region region;
        line_stream >> region.chromosome;
        line_stream >> region.start_position;
        line_stream >> region.end_position;

        if( line_stream.fail() )
            throw runtime_error( "malformed bam on line " + to_string( line_number ) );
        else
            regions.push_back( move( region ) );

        ++line_number;
    }

    return regions;
}

