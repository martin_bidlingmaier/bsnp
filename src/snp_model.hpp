#ifndef SNP_MODEL_HPP_ 
#define SNP_MODEL_HPP_ 

#include "base_types.hpp"
#include "methylation_probability.hpp"
#include "genotype_prior_probabilities.hpp"

#include <numeric>
#include <array>
#include <algorithm>

double calculate_epsilon( char base_quality );
// calculate Pr( D_j | B ), i.e.
// calculate the probability of observing base dj given the real base b (habloid)
// see paper for details
double pr_dj_given_b( base dj , strand strand , base b , double alpha , double beta, double gamma , double epsilon );


struct snp_test_result
{
    genotype most_likely_genotype;
    
    double error_probability;
    double no_variant_probability;
    double mapping_quality;

    std::array< size_t , base::number() > base_specific_depths;
    size_t sample_depth;
};

template< class BaseInfoIterator >
snp_test_result test_snp( BaseInfoIterator d_begin , BaseInfoIterator d_end , std::array< optional_base , 5 > reference_context , double alpha , double gamma )
{
    using namespace std;
    
    // store number of occurrences of a specific base in sample
    array< size_t , base::number() > base_specific_depths;
    base_specific_depths.fill( 0 );
 
    optional_base ref_base = reference_context[ 2 ];
    const array< double , genotype::number() >& prior_probabilities = genotype_prior_probabilities( ref_base );

    // for each base, calculate methylation probability if the current position was that base
    array< double , base::number() > betas;
    betas[ base::A() ] = 0.0; // beta is irrelevant for A
    betas[ base::T() ] = 0.0; // and for T
    
    reference_context[ 2 ] = base::C(); 
    betas[ base::C() ] = get_beta( reference_context);
    reference_context[ 2 ] = base::G(); 
    betas[ base::G() ] = get_beta( reference_context );
    
    array< pair< double , int > , genotype::number() > pr_d_given_g_times_pr_d_s;
    // Pr( D | G ) * Pr( D )    for each genotype, see paper 
    // we have 
    //      
    //      Pr( D | G ) * Pr( D ) = PI( G ) * Pr( D | G ) = PI( G ) * product( j=1...r  , Pr( D_j | G ) )
    //
    // 's' at the end of the variable name is for plural
    // initialise with PI( G ) and multiply by Pr( D_j | G ) later
    //
    // These values can get very small (too small for double), if r is very big.
    // Since only a quotient of these values is needed, every pr_d_given_g_times_pr_d is represented by a
    // significand double in the range [0.5,1) and an integral exponent, such that
    //
    //      pr_dj_given_g_times_pr_d = significand * 2 ^ exponent
    //
    // initilize pr_d_given_g_times_pr_d_s with genotype prior probabilities and normalize
    for( size_t i = 0 ; i != pr_d_given_g_times_pr_d_s.size() ; ++i )
    {
        pair< double , int >& normalized_double = pr_d_given_g_times_pr_d_s[ i ];
        double& significand = normalized_double.first;
        int& exponent = normalized_double.second;

        significand = frexp( prior_probabilities[ i ] , &exponent );
    }

    double square_mapping_quality_sum = 0.0;
    
    // the following loop is for calculating the product in the above formula and the rms of the mapping qualities
    for( auto d_it = d_begin ; d_it != d_end ; ++d_it )
    {
        const base_info& dj = *d_it;

        square_mapping_quality_sum += dj.mapping_quality * dj.mapping_quality;

        ++base_specific_depths[ dj.b ];

        double epsilon = calculate_epsilon( dj.base_quality );
        // for each base b, calculate pr_dj_given_b once and save it
        array< double , base::number() > pr_dj_given_b_s;
        for( base b : base::values() )
            pr_dj_given_b_s[ b ] = pr_dj_given_b( dj.b , dj.st , b , alpha , betas[ b ] , gamma , epsilon );

        // update pr_d_given_g as a product of each pr_dj_given_g
        for( genotype g : genotype::values() )
        {
            // calculate Pr( D_j | G ), i.e.
            // calulate the probability of observing base dj given the real base pair G=AB
            // see paper for details
            double pr_dj_given_g = 0.5 * ( pr_dj_given_b_s[ g.a() ] + pr_dj_given_b_s[ g.b() ] );
            
            // update the product, see formula above
            pair< double , int >& pr_d_given_g_times_pr_d = pr_d_given_g_times_pr_d_s[ g ];
            
            // multiply significand
            pr_d_given_g_times_pr_d.first *= pr_dj_given_g;
            // and normalize the double again
            int exponent;
            pr_d_given_g_times_pr_d.first = frexp( pr_d_given_g_times_pr_d.first , &exponent );
            pr_d_given_g_times_pr_d.second += exponent;
        }
    }
    
    int max_exponent = numeric_limits< int >::min();
    for( pair< double , int >& normalized_double : pr_d_given_g_times_pr_d_s )
        max_exponent = max( max_exponent , normalized_double.second );
    
    array< double , genotype::number() > reduced_pr_d_given_g_times_pr_d_s;
    for( int i = 0 ; i != genotype::number() ; ++i )
    {
        double& reduced_value = reduced_pr_d_given_g_times_pr_d_s[ i ];
        pair< double , int >& normalized_value = pr_d_given_g_times_pr_d_s[ i ];
        
        reduced_value = ldexp( normalized_value.first , normalized_value.second - max_exponent );
    }


    // get iterator to maximum (G varying) for Pr( D | G ) * Pr( D )
    auto max_it = max_element( reduced_pr_d_given_g_times_pr_d_s.begin() , reduced_pr_d_given_g_times_pr_d_s.end() );
    
    // calculate Pr( D ), which just the sum over 
    //      
    //      PI( G ) * Pr( D | G ) )
    //
    // over each genotype G
    double pr_d = accumulate( reduced_pr_d_given_g_times_pr_d_s.begin() , reduced_pr_d_given_g_times_pr_d_s.end() , 0.0 );
    double most_likely_genotype_probability = *max_it / pr_d;
    // get genotype with highest probability
    genotype most_likely_genotype = genotype( max_it - reduced_pr_d_given_g_times_pr_d_s.begin() );
    // calculate error probability
    double error_probability = 1 - most_likely_genotype_probability;
    // calculate probability of 'no variant at this position'
    double no_variant_probability;
    if( is_base( ref_base ) )
    {
        genotype no_variant_gt = genotype( base( ref_base ) , base( ref_base ) );
        no_variant_probability = reduced_pr_d_given_g_times_pr_d_s[ no_variant_gt ];
    }
    else
        no_variant_probability = 0.0;
    // get sample depth
    size_t sample_depth = distance( d_begin , d_end );
    // calculate rms of mapping qualities
    double mapping_quality = sqrt( square_mapping_quality_sum / sample_depth );
    return snp_test_result
    {
        most_likely_genotype,
        
        error_probability,
        no_variant_probability,
        mapping_quality,

        base_specific_depths,
        sample_depth
    };
}

#endif

