#ifndef SNP_HPP_
#define SNP_HPP_

#include "base_types.hpp"

#include <ostream>
#include <istream>

struct snp_test_result;

struct snp
{
    snp( std::string chromosome_name , size_t pos , optional_base reference_base , const snp_test_result& snp_test );

    std::string chromosome_name;
    int32_t chromosome_index;
    int32_t position;
    optional_base reference_base;

    const char* id;
    
    double no_variant_probability;
    double error_probability;
    
    size_t sample_depth;
    
    double mapping_quality;

    struct base_specific_data
    {
        base b;
        
        // base_frequency is the ratio 
        //      
        //      #(observation of this base) / sample_depth
        //
        double base_frequency;
        // number of alleles with this base (so 1 or 2)
        size_t allele_count;
        // number of observations of this base in sample
        size_t base_specific_depth;
    };
    base_specific_data first_base;
    bool first_base_is_ref() const;
    bool has_second_base;
    base_specific_data second_base;
};

// write vcf line
std::ostream& operator<<( std::ostream& os , const snp& snp );

#endif

