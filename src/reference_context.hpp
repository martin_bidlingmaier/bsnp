#ifndef REFERENCE_CONTEXT_HPP_
#define REFERENCE_CONTEXT_HPP_

#include "base_types.hpp"

#include <string>
#include <array>
#include <algorithm>
#include <iterator>

template< class ReferenceIterator >
class reference_context
{
public:
    reference_context( ReferenceIterator reference_begin , ReferenceIterator reference_end )
    {
        ref_begin = reference_begin;
        ref_end = reference_end;
        ref_pos = reference_end;
    }
    // jump to 'position' within reference sequence and update current_context accordingly
    void jump_to_position( size_t position )
    {
        using namespace std;
        
        current_context.fill( optional_base::N() );
        

        // depending on the start of the region and the length of the reference sequence,
        // only parts of the current_context can be updated
        auto context_it = current_context.begin();
        ref_pos = ref_begin;
        
        if( position < 2 )
            advance( context_it , 2 - position ); // no context somewhere on the left available for the first base
        else
        {
            // region starts further to the right, need to update start within reference sequence
            size_t ref_length = distance( ref_begin , ref_end );
            // is region start context still whithin reference?
            if( position - 2 > ref_length )
                ref_pos = ref_end; // if not, set ref_pos to end
            else
                advance( ref_pos , position - 2 ); // otherwise, advance ref_pos accordingly
        }
        
        // fill reference context for first position
        while( context_it != current_context.end() && ref_pos != ref_end )
        {
            *context_it = *ref_pos;

            ++context_it;
            ++ref_pos;
        }
    }
    // increment position within reference and update current_context accordingly
    void increment_position()
    {
        shift_current_context_left();
        if( ref_pos != ref_end )
        {
            current_context.back() = *ref_pos;
            ++ref_pos;
        }
        else
            current_context.back() = optional_base::N();
    }
        
    const std::array< optional_base , 5 >& get_current_context() const
    {
        return current_context;
    }
    optional_base get_current_base() const
    {
        return current_context[ 2 ];
    }
private:
    void shift_current_context_left()
    {
        using namespace std;

        auto from_begin = current_context.begin() + 1;
        auto from_end = current_context.end();

        auto to_begin = current_context.begin();

        copy( from_begin , from_end , to_begin );
    }


    ReferenceIterator ref_pos; // this will actually always point to the first position that is not in the current context
    ReferenceIterator ref_begin;
    ReferenceIterator ref_end;

    std::array< optional_base , 5 > current_context;
};

#endif

