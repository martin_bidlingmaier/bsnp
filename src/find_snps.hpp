#ifndef FIND_SNPS_HPP_
#define FIND_SNPS_HPP_

#include "base_types.hpp"
#include "snp_model.hpp"
#include "positional_bases.hpp"
#include "methylation_probability.hpp"
#include "reference_context.hpp"

#include <api/BamReader.h>
#include <api/BamAlignment.h>

#include <utility>
#include <iterator>
#include <algorithm>

struct genome_region
{
    int32_t sequence_id;
    int32_t start_position;
    int32_t end_position;
};

struct snp_config
{
    double alpha;
    double gamma;

    double minimum_confidence;
    
    size_t minimum_mapping_quality;
    size_t minimum_base_quality;

    size_t maximum_sample_size;

    aligner_t aligner;
};

template< class SNPFunctor , class ReferenceContextType >
void find_snps( BamTools::BamReader& bam_file , ReferenceContextType& reference , const snp_config& config , const genome_region& region , SNPFunctor&& snp_functor )
{
    using namespace std;
    
    assert( region.start_position <= region.end_position );
    auto base_filter = [&]( const base_info& info ) -> bool
    {
        return info.base_quality >= config.minimum_base_quality;
    };
    auto alignment_filter = [&]( const BamTools::BamAlignment& alignment ) -> bool
    {
        bool is_mapped = alignment.IsMapped();
        bool quality_ok = alignment.MapQuality >= config.minimum_mapping_quality;
        bool is_primary = alignment.IsPrimaryAlignment();
        
        return is_mapped && quality_ok && is_primary;
    };
    positional_bases bases( bam_file , config.aligner , base_filter , alignment_filter );
    if( !bases.jump_to_position( region.sequence_id , region.start_position ) )
        return; // unfortunately, the api doesn't tell whether an IO error occured or there was no alignment (the second case is ok for us, the first one isn't)

    reference.jump_to_position( region.start_position );

    // walk through each position (with positional_bases) and check for snp
    while( bases.position() < region.end_position )
    {
        positional_bases::iterator sample_begin = bases.begin();
        positional_bases::iterator sample_end = bases.end();
        // down sampling if needed
        if( distance( sample_begin , sample_end ) > config.maximum_sample_size )
        {
            random_shuffle( sample_begin , sample_end );
            sample_end = sample_begin + config.maximum_sample_size;
        }
        snp_test_result test_result = 
            test_snp( sample_begin , sample_end , reference.get_current_context() , config.alpha , config.gamma );
        if( test_result.error_probability <= 1 - config.minimum_confidence )
        {
            if( is_base( reference.get_current_base() ) )
            {
                base ref_base = base( reference.get_current_base() );
                bool is_snp = test_result.most_likely_genotype != genotype( ref_base , ref_base ); 
                if( is_snp )
                    snp_functor( bases.position() , reference.get_current_base() , test_result );
            }
            else
                snp_functor( bases.position() , reference.get_current_base() , test_result );
        }

        bases.increment_position();
        reference.increment_position();
    }
}

#endif

