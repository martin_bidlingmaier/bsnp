#ifndef POSITIONAL_BASES_HPP_
#define POSITIONAL_BASES_HPP_

#include "base_types.hpp"

#include <vector>
#include <deque>
#include <functional>
#include <stdexcept>

#include <api/BamAlignment.h>
#include <api/BamReader.h>

namespace ALIGNER
{

enum aligner_t
{
    BISMARK,
    BSMAP
};

}

using ALIGNER::aligner_t;


class positional_bases
{
public:
    positional_bases( BamTools::BamReader& bam_reader , aligner_t aligner , std::function< bool ( const base_info& ) > base_filter_func , std::function< bool ( const BamTools::BamAlignment& ) > alignment_filter_func );
    positional_bases( const positional_bases& ) = delete;
    positional_bases& operator=( const positional_bases& ) = delete;
    
    // position index is zero based
    int32_t position() const;
    bool jump_to_position( int32_t new_reference_id , int32_t new_position );
    bool increment_position();
    // retrieve iterators to the range of bases at the current position
    typedef std::vector< base_info >::iterator iterator;
    iterator begin();
    iterator end();

    struct malformed_bam_exception 
      : std::runtime_error
    {
        // forward base class constructor
        using std::runtime_error::runtime_error;
    };
private:
    bool read_alignments();
    void add_bases( const BamTools::BamAlignment& alignment );
 

    BamTools::BamReader& bam;
    aligner_t aligner; // aligner used to create bam file

    // pos and ref_id describe current position
    int32_t pos; // current position whitin reference for current sequence
    int32_t ref_id; // current sequence
    
    // user-supplied function to filter alignments
    std::function< bool ( const base_info& ) > base_filter;
    std::function< bool ( const BamTools::BamAlignment& ) > alignment_filter;
    
    // position of the last alignment that was read
    int32_t last_alignment_pos;
    // container for bases at a specific position
    // e.g.
    // bases_for_positions[ 0 ] contains bases for the current position,
    // bases_for_positions[ 15 ] contains bases for the position 15 ahead
    // only bases_for_positions[ 0 ] is complete, there may be missing alignments for higher positions
    std::deque< std::vector< base_info > > bases_for_positions;
};

#endif

