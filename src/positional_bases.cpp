#include "positional_bases.hpp"

#include <vector>
#include <sstream>
#include <deque>
#include <limits>

#include <api/BamAlignment.h>
#include <api/BamReader.h>

using namespace std;
using namespace BamTools;

positional_bases::positional_bases( BamReader& bam_reader , aligner_t al , function< bool ( const base_info& ) > base_filter_func , function< bool ( const BamAlignment& ) > alignment_filter_func )
  : bam( bam_reader ) ,
    base_filter( std::move( base_filter_func ) ) ,
    alignment_filter( std::move( alignment_filter_func ) )
{
    aligner = al;

    pos = -1;
    ref_id = -1;

    last_alignment_pos = 0;
}

// position index is zero based
int32_t positional_bases::position() const
{
    return pos;
}
bool positional_bases::jump_to_position( int32_t new_reference_id , int32_t new_position )
{
    if( bam.Jump( new_reference_id , new_position ) )
    {
        ref_id = new_reference_id;
        pos = new_position;

        last_alignment_pos = 0;
        bases_for_positions.clear();

        return read_alignments();
    }
    else
        return false;
}
bool positional_bases::increment_position()
{
    ++pos;
    bases_for_positions.front().clear();
    bases_for_positions.push_back( move( bases_for_positions.front() ) );
    bases_for_positions.pop_front();
    return read_alignments();
}
// retrieve iterators to the range of bases at the current position
positional_bases::iterator positional_bases::begin()
{
    return bases_for_positions.front().begin();
}
positional_bases::iterator positional_bases::end()
{
    return bases_for_positions.front().end();
}

// read alignments until the next reference begins after the current position
// this last alignment is then stored in next_alignment
// returns true, if some alignments could be read (not necessarily matching the current position)
// if no alignments are left in the file (eof) or an io error occured, false is returned
bool positional_bases::read_alignments()
{  
    if( last_alignment_pos == numeric_limits< int32_t >::max() )
        return false;

    // true if at least one alignment was read from the bam file
    bool alignment_read = false;

    while( last_alignment_pos <= pos )
    {
        BamAlignment current_alignment;
        if( !bam.GetNextAlignment( current_alignment ) )
        {
            // make last_alignment_pos invalid and report EOF/IO-Error
            last_alignment_pos = numeric_limits< int32_t >::max();
            return alignment_read;
        }
        else
            alignment_read = true;

        if( current_alignment.RefID == ref_id )
        {
            // the current alignment belongs to the current reference
            // update last_alignment_pos
            last_alignment_pos = current_alignment.Position;
            // test if current_alignment passes the user-supplied test and add bases
            if( alignment_filter( current_alignment ) )
                add_bases( current_alignment );
        }
        else
        {
            // alignment does not belong to reference anymore, make last_alignment_pos invalid and report it
            last_alignment_pos = numeric_limits< int32_t >::max();
            return false;
        }
    }
    return true;
}

strand get_strand( const BamAlignment& alignment , aligner_t aligner )
{
    if( aligner == ALIGNER::BISMARK )
    {
        if( alignment.IsReverseStrand() )
            return strand::MINUS;
        else
            return strand::PLUS;
    }
    else if( aligner == ALIGNER::BSMAP )
    {
        if( alignment.IsPaired() )
        {
            if( alignment.IsFirstMate() )
            {
                if( alignment.IsReverseStrand() )
                    return strand::MINUS;
                else
                    return strand::PLUS;
            }
            else
            {
                if( alignment.IsReverseStrand() )
                    return strand::PLUS;
                else
                    return strand::MINUS;
            }
        }
        else
        {
            if( alignment.IsReverseStrand() )
                return strand::MINUS;
            else
                return strand::PLUS;
        }
    }
    
    assert( false ); // 'aligner' should be one of the values in enum
    return strand::PLUS;
}


void positional_bases::add_bases( const BamAlignment& alignment )
{
    static const char* NON_MATCHING_CIGAR = "cigar string does not match length of alignment";

    strand strand = get_strand( alignment , aligner );

    const auto& alignment_bases_container = alignment.QueryBases;
    const auto& alignment_qualities_container = alignment.Qualities;

    if( alignment_bases_container.size() != alignment_qualities_container.size() )
        throw malformed_bam_exception( "base number not equal to base qualities number" );

    // iterator over the bases in alignment
    auto alignment_bases_it = alignment_bases_container.begin();
    auto alignment_qualities_it = alignment_qualities_container.begin();
    // function to check whether the current position within alignment is still valid
    auto alignment_pos_valid = [&]() -> bool
    {
        // alignment_bases_container.size() == alignment_qualities_container.size(), so we only have to check for one iterator being invalid
        return alignment_bases_it != alignment_bases_container.end();
    };
    // increment both iterators that refer to alignment
    auto incr_alignment_pos = [&]()
    {
        if( !alignment_pos_valid() )
            throw malformed_bam_exception( NON_MATCHING_CIGAR );

        ++alignment_bases_it;
        ++alignment_qualities_it;
    };


    // current position within reference, relative to the alignment
    int current_relative_pos = alignment.Position - pos;
    if( (int) bases_for_positions.size() <= current_relative_pos )
        bases_for_positions.resize( current_relative_pos + 1 );
    // increment base_it, update current_relative pos accordingly and make sure that bases_it != bases.end()
    auto incr_ref_pos = [&]()
    {
        ++current_relative_pos;
        if( (int) bases_for_positions.size() <= current_relative_pos )
            bases_for_positions.emplace_back();
    };

    for( auto cigar_op : alignment.CigarData )
    {
        switch( cigar_op.Type )
        {
            case 'M':
            case '=':
            case 'X':
                for( size_t i = 0 ; i != cigar_op.Length ; ++i )
                {
                    if( current_relative_pos >= 0 && alignment_pos_valid() )
                    {
                        optional_base opt_b = to_base( *alignment_bases_it );
                        if( is_base( opt_b ) )
                        {
                            base b( opt_b );
                            if( *alignment_qualities_it < 33 )
                                throw malformed_bam_exception( "invalid base quality (too low): " + *alignment_qualities_it );
                            uint8_t base_quality = (uint8_t)*alignment_qualities_it - 33;
                            uint16_t mapping_quality = alignment.MapQuality;
                            base_info info = { b , strand ,base_quality , mapping_quality };
                            if( base_filter( info ) )
                                bases_for_positions[ current_relative_pos ].push_back( info );
                        }
                    }

                    incr_alignment_pos();
                    incr_ref_pos();
                }
                break;
            case 'I':
            case 'S':
                for( size_t i = 0 ; i != cigar_op.Length; ++i )
                    incr_alignment_pos();
                break;
            case 'D':
            case 'N':
                for( size_t i = 0 ; i != cigar_op.Length ; ++i )
                    incr_ref_pos();
                break;
            case 'H': // TODO: what do 'H' and 'P' mean? 
            case 'P':
            default:
                throw malformed_bam_exception( "invalid or unsupported cigar op: " + cigar_op.Type );
                break;
        }
    }

    // make sure all bases in alignment have been consumed
    if( alignment_pos_valid() )
        throw malformed_bam_exception( NON_MATCHING_CIGAR ); 
}

