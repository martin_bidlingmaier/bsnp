#include "call_snps.hpp"

#include "bed_utils.hpp"
#include "fasta_file.hpp"
#include "snp.hpp"
#include "db_snp.hpp"

#include <string>
#include <set>

using namespace std;
using namespace BamTools;

map< string , size_t > extract_sequence_ids( const BamReader& bam_reader )
{
    map< string , size_t > result;
    for( const RefData& ref_data : bam_reader.GetReferenceData() )
    {
        const string& name = ref_data.RefName;
        int id = bam_reader.GetReferenceID( name );
        assert( id >= 0 );
        result[ name ] = (size_t)id;
    }
    return result;
}

bool bed_region_compare( const bed_region& first , const bed_region& second )
{
    return first.chromosome < second.chromosome;
}

// does a found snp match a dbsnp entry?
// this does not check whether chromosomes match as this information is not in db_snp::entry
bool matches_entry( const snp& snp , const db_snp::entry& db_entry )
{
    if( (uint32_t)snp.position != db_entry.position )
        return false;
    if( snp.reference_base != db_entry.reference_base )
    {
        cerr << "Warning: reference base in dbsnp and fasta do not matched at zero-based position " << db_entry.position << endl;
        return false;
    }
    
    // check whether one of the two bases listed in snp are also in alternatives of db_snp entry
    bool first_base_in_alternatives = 
            count( db_entry.alternatives.begin() , db_entry.alternatives.end() , snp.first_base.b ) != 0;
    bool second_base_in_alternatives = 
            count( db_entry.alternatives.begin() , db_entry.alternatives.end() , snp.second_base.b ) != 0;
    
    return first_base_in_alternatives || second_base_in_alternatives;
}

void call_snps( ostream& output_vcf_file , const snp_config& config , BamReader& bam_reader , indexed_fasta_file& fasta , vector< bed_region > bed_regions , const db_snp& known_snps )
{
    // create mapping between sequence names and their ids 
    map< string , size_t > sequence_ids = extract_sequence_ids( bam_reader );

    // sort bed_regions for chromosome, so bed regions in the same sequence are processed together
    sort( bed_regions.begin() , bed_regions.end() , bed_region_compare );

    const string* current_chromosome = 0;
    vector< optional_base > current_reference_sequence;
    size_t current_sequence_id;
    const vector< db_snp::entry >* current_known_snps = 0;
    vector< db_snp::entry >::const_iterator known_snps_pos;
    
    for( const bed_region& bed_region : bed_regions )
    {
        if( current_chromosome == 0 || *current_chromosome != bed_region.chromosome )
        {
            // if the chromosome for this bed_region is different from the last one, load new reference
            current_chromosome = &bed_region.chromosome;

            // find sequence id for new chromosome name
            auto sequence_id_it = sequence_ids.find( bed_region.chromosome );
            if( sequence_id_it == sequence_ids.end() )
                throw runtime_error( "could not find sequence in bam file: \"" + bed_region.chromosome + "\"");
            current_sequence_id = sequence_id_it->second;
 
            // get iterators to reference sequence
            current_reference_sequence = fasta.load_sequence( bed_region.chromosome );

            // set known snps for the currently processed sequence
            auto it = known_snps.entries.find( "chr" + bed_region.chromosome );
            if( it == known_snps.entries.end() )
                it = known_snps.entries.find( bed_region.chromosome );
            if( it != known_snps.entries.end() )
            {
                current_known_snps = &it->second;
                known_snps_pos = current_known_snps->begin();
            }
            else
                current_known_snps = 0;
        }

        genome_region region;
        region.sequence_id = current_sequence_id;
        region.start_position = bed_region.start_position;
        region.end_position = bed_region.end_position;

        auto ref_begin = current_reference_sequence.begin();
        auto ref_end = current_reference_sequence.end();
        reference_context< vector< optional_base >::iterator > reference_context( ref_begin , ref_end );

        // callback for found snps
        auto snp_callback = [&]( size_t pos , optional_base ref_base , const snp_test_result& test_result )
        {
            snp snp( bed_region.chromosome , pos , ref_base , test_result );
            if( current_known_snps != 0 )
            {
                // information about known snps for this sequence available
                // increment position until there are no more known snps or the next known snp's position is not lower than the current position
                while( known_snps_pos != current_known_snps->end() && known_snps_pos->position < pos )
                    ++known_snps_pos;
                
                // check whether current dbsnp entry matches snp
                if( known_snps_pos != current_known_snps->end() && matches_entry( snp , *known_snps_pos ) )
                    snp.id = known_snps.id_strings_container.data() + known_snps_pos->id_start_index;
            }
            output_vcf_file << snp << "\n";
        };
        
        find_snps( bam_reader , reference_context , config , region , snp_callback );
    }
}

// version without specific regions
void call_snps( ostream& output_vcf_file , const snp_config& config , BamReader& bam_reader , indexed_fasta_file& fasta , const db_snp& known_snps )
{
    // this will be the full sequences that have a reference in the fasta file as well as alignments in the bam file
    vector< bed_region > regions;

    // mapping sequence name -> ( "zero based start position" , "sequence length" )
    const map< string , pair< size_t , size_t > >& fasta_index = fasta.get_index();
    
    // names of sequences in bam file
    set< string > bam_sequences;
    for( const RefData& ref_data : bam_reader.GetReferenceData() )
        bam_sequences.insert( ref_data.RefName );
    
    for( const pair< string , pair< size_t , size_t > >& reference_sequence : fasta_index )
    {
        const string& sequence_name = reference_sequence.first;
        size_t sequence_length = reference_sequence.second.second;

        if( bam_sequences.find( sequence_name ) != bam_sequences.end() )
        {
            bed_region region;
            region.chromosome = sequence_name;
            region.start_position = 0;
            region.end_position = sequence_length;
            regions.push_back( region );
        }
    }

    call_snps( output_vcf_file , config , bam_reader , fasta , regions , known_snps );
}

