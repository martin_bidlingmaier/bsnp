#ifndef VCF_UTILS_HPP_
#define VCF_UTILS_HPP_

#include <ostream>
#include <string>
#include <vector>

#include <api/SamHeader.h>

#include "db_snp_entry.hpp"

void write_vcf_header( std::ostream& os , const BamTools::SamHeader& sam_header , const std::string& reference_file_name );

#endif

