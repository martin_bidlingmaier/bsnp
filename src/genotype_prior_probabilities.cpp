#include "genotype_prior_probabilities.hpp"

using namespace std;

// create table of probabilities for each genotype given the reference base
// this function is overloaded for an optional_base as reference_base (further down)
array< double , genotype::number() > make_genotype_prior_probabilities( base reference_base )
{
    // "import" base names
    constexpr base A = base::A();
    constexpr base C = base::C();
    constexpr base G = base::G();
    constexpr base T = base::T();
    typedef genotype g;
    
    // prior probabilites if reference base is "G"
    array< double , genotype::number() > g_probabilities;
    g_probabilities[ g( A , A ) ] = 3.33e-4;
    g_probabilities[ g( A , C ) ] = 1.11e-7;
    g_probabilities[ g( A , G ) ] = 6.67e-4;
    g_probabilities[ g( A , T ) ] = 1.11e-7;

    g_probabilities[ g( C , C ) ] = 8.33e-5;
    g_probabilities[ g( C , G ) ] = 1.67e-4;
    g_probabilities[ g( C , T ) ] = 2.78e-8;


    g_probabilities[ g( G , G ) ] = 0.9985;
    g_probabilities[ g( G , T ) ] = 1.67e-4;

    g_probabilities[ g( T , T ) ] = 8.33e-5;

    
    // create prior probabilites for the reference base using the table for G
    // 
    // what base_mapping is supposed to do:
    // Let X, Y be bases. Then, 
    //
    //      g_probabilities[ genotype( X , Y ) ]
    // equals
    //      prior_probabilites[ genotype( base_mapping( X ) , base_mapping( Y ) ) ]
    // 
    // for the actual reference base.
    // for example, base_mapping is the identity if reference_base == G
    array< base , base::number() > base_mapping;
    switch( reference_base )
    {
    case A:
        // swap A and G, C and T can stay the same
        base_mapping[ A ] = G;
        base_mapping[ C ] = C;
        base_mapping[ G ] = A;
        base_mapping[ T ] = T;
        break;
    case C:
        // C and T take the places of A and G, and G should map to C
        base_mapping[ A ] = T;
        base_mapping[ C ] = G;
        base_mapping[ G ] = C;
        base_mapping[ T ] = A;
        break;
    case G:
        // identity
        base_mapping[ A ] = A;
        base_mapping[ C ] = C;
        base_mapping[ G ] = G;
        base_mapping[ T ] = T;
        break;
    case T:
        // analogous to the C case, but G has to map to T
        base_mapping[ A ] = C;
        base_mapping[ C ] = G;
        base_mapping[ G ] = T;
        base_mapping[ T ] = A;
        break;
    default:
        assert( false );
    }

    array< double , genotype::number() > result;
    for( genotype g : genotype::values() )
        result[ genotype( base_mapping[ g.a() ] , base_mapping[ g.b() ] ) ] = g_probabilities[ g ];
    return result;
}

array< double , genotype::number() > make_genotype_prior_probabilities( optional_base reference_base )
{
    if( is_base( reference_base ) )
        return make_genotype_prior_probabilities( base( reference_base ) );

    // reference_base == N
    // assume each base is equally likely,
    // then prior_probability[ g ] is a quarter of the sum of prior_probability[ g ] for each possible reference base
    array< double , genotype::number() > result;
    result.fill( 0.0 );
    for( base ref_base : base::values() )
    {
        // this is not a recursive call as it will match the other overload
        array< double , genotype::number() > priors_for_current_base = make_genotype_prior_probabilities( ref_base );
        for( genotype g : genotype::values() )
            result[ g ] += priors_for_current_base[ g ];
    }
    for( genotype g : genotype::values() )
        result[ g ] /= base::number();

    return result;
}

// create a table that contains genotype_prior_probabilities for each (optional_base) reference_base
// not plural s: make_genotypeS_prior...
array< array< double , genotype::number() > , optional_base::number() > make_genotypes_prior_probabilities()
{
    array< array< double , genotype::number() > , optional_base::number() > result;
    for( optional_base ref_base : optional_base::values() )
        result[ ref_base ] = make_genotype_prior_probabilities( ref_base );
    return result;
}

// actual table that contains genotype_prior_probabilities
const array< array< double , genotype::number() > , optional_base::number() > genotype_prior_probabilities_table = 
        make_genotypes_prior_probabilities();

const array< double , genotype::number() >& genotype_prior_probabilities( optional_base reference_base )
{
    return genotype_prior_probabilities_table[ reference_base ];
}
