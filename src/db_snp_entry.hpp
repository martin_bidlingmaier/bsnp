#ifndef DB_SNP_ENTRY_HPP_
#define DB_SNP_ENTRY_HPP_

#include "base_types.hpp"

#include <array>
#include <string>
#include <istream>
#include <map>

class db_snp_entry
{
public:
    db_snp_entry();
    db_snp_entry( const db_snp_entry& entry );
    db_snp_entry( db_snp_entry&& entry );
    ~db_snp_entry();

    void swap( db_snp_entry& entry );
    db_snp_entry& operator=( db_snp_entry entry );
    
    uint32_t position;
    uint8_t chromosome_index;
    optional_base reference_base;
    
    const char* get_id() const;
    void set_id( const std::string& str );
private:
    bool has_long_id;
    union
    {
        std::array< char , 12 > small_string;
        char* long_string;
    } id;

    void clean();
};

namespace std
{

template<>
inline void swap( db_snp_entry& a , db_snp_entry& b )
{
    a.swap( b );
}

}

bool operator<( const db_snp_entry& a , const db_snp_entry& b );
// get next entry from vcf file
bool read_next_entry( std::istream& is , db_snp_entry& entry , const std::map< std::string , uint8_t > &sequence_ids );

#endif

