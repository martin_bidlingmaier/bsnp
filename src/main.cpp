#include <iostream>
#include <limits>
#include <stdexcept>

#include "parse_input.hpp"
#include "call_snps.hpp"
#include "vcf_utils.hpp"
#include "bed_utils.hpp"

using namespace std;
using namespace BamTools;

int main( int argc , char **argv )
{
    try
    {
        input inp = parse_input( argc , argv );

        BamReader bam_reader;
        if( !bam_reader.Open( inp.bam_path ) )
            throw runtime_error( "unable to open bam file " + inp.bam_path );

        string bam_index_path = inp.bam_path + ".bai";
        if( !bam_reader.OpenIndex( bam_index_path ) )
            throw runtime_error( "unable to open bam index file " + bam_index_path );

        ifstream fasta_file( inp.fasta_path , ios::binary );
        if( fasta_file.fail() )
            throw runtime_error( "unable to open fasta file " + inp.fasta_path );

        string fasta_index_path = inp.fasta_path + ".fai";
        ifstream fasta_index_file( fasta_index_path );
        if( fasta_index_file.fail() )
            throw runtime_error( "unable to open fasta index file " + fasta_index_path );
        indexed_fasta_file fasta( fasta_file , fasta_index_file );
        
        unique_ptr< vector< bed_region > > regions;
        if( inp.bed_path )
        {
            ifstream bed_file( *inp.bed_path );
            if( bed_file.fail() )
                throw runtime_error( "unable to open bed file " + *inp.bed_path );
            regions = unique_ptr< vector< bed_region > >( new vector< bed_region >( load_bed( bed_file ) ) );
        }

        ofstream vcf_file( inp.vcf_path , ios::trunc );
        if( vcf_file.fail() )
            throw runtime_error( "unable to create file " + inp.vcf_path );

        db_snp known_snps;
        if( inp.dbsnp_path )
        {
            ifstream dbsnp_file( *inp.dbsnp_path );
            if( dbsnp_file.fail() )
                throw runtime_error( "unable to open dbSNP file " + *inp.dbsnp_path );
            known_snps = load_db_snp( dbsnp_file );
        }

        write_vcf_header( vcf_file , bam_reader.GetHeader() , inp.fasta_path );
        if( regions )
            call_snps( vcf_file , inp.config , bam_reader , fasta , move( *regions ) , known_snps );
        else
            call_snps( vcf_file , inp.config , bam_reader , fasta , known_snps );
    }
    catch( const exception& exc )
    {
        cerr << "usage: [OPTIONS] BAM FASTA VCF" << endl;
        cerr << exc.what() << endl;
    }
}
