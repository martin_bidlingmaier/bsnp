#ifndef PARSE_INPUT_HPP_
#define PARSE_INPUT_HPP_

#include "find_snps.hpp"

#include <string>
#include <memory>

struct input
{
    std::string bam_path;
    std::string fasta_path;
    std::string vcf_path;

    std::unique_ptr< std::string > bed_path;
    std::unique_ptr< std::string > dbsnp_path;

    snp_config config;
};

input parse_input( int argc , char** argv );

#endif

