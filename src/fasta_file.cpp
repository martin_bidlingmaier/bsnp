#include "fasta_file.hpp"

#include <cctype>
#include <sstream>


#include <iostream> // todo 

using namespace std; 

indexed_fasta_file::indexed_fasta_file( istream& fasta , istream& index )
  : fasta_file( fasta )
{
    // load index
    string line;
    while( getline( index , line ) )
    {
        istringstream line_stream( line );
        
        string name;
        line_stream >> name;
        
        size_t size;
        line_stream >> size;
        
        size_t location;
        line_stream >> location;

        if( line_stream )
            fasta_index[ name ] = make_pair( location , size );
    }
}
vector< string > indexed_fasta_file::sequences() const
{
    vector< string > result;
    for( auto elem : fasta_index )
        result.push_back( elem.first );

    return result;
}
const map< string , pair< size_t , size_t > >& indexed_fasta_file::get_index() const
{
    return fasta_index;
}
vector< optional_base > indexed_fasta_file::load_sequence( const string& sequence_name )
{
    auto index_it = fasta_index.find( sequence_name );
    if( index_it == fasta_index.end() )
        throw runtime_error( "unable to find reference sequence: " + sequence_name );
    
    size_t begin_pos = index_it->second.first;
    size_t size = index_it->second.second;

    fasta_file.seekg( begin_pos );
    if( !fasta_file.good() )
        throw runtime_error( "corrupted fasta file/index" );
    vector< optional_base > result;
    result.reserve( size );

    string line;
    while( getline( fasta_file , line ) && ( line.empty() || line.front() != '>' ) )
    {
        for( char c : line )
            result.push_back( to_base( c ) );
    }

    if( !result.size() == size )
        throw runtime_error( "corrupted fasta file/index" );

    return result;
}


