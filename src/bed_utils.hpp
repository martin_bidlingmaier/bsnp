#ifndef BED_UTILS_HPP_
#define BED_UTILS_HPP_

#include <istream>
#include <string>
#include <vector>

struct bed_region
{
    std::string chromosome;
    // zeros based positions:
    size_t start_position;
    size_t end_position;
};

std::vector< bed_region > load_bed( std::istream& bed_stream );

#endif

