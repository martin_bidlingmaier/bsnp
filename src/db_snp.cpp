#include "db_snp.hpp"

#include <algorithm>
#include <sstream>
#include <stdexcept>
#include <iostream>

using namespace std;


struct db_snp_line_info
{
    string sequence_name;
    uint32_t position;
    string snp_id;
    base reference_base;
    array< optional_base , 3 > alternatives;
};

struct malformed_snp
  : runtime_error
{
    using runtime_error::runtime_error;
};
// returns true if line describes an snp (s(!)np), false otherwise
// throws malformed_snp for syntax errors
bool read_snp_line( istream& stream , db_snp_line_info& output )
{
    string buffer;

    auto get_column = [&]( string& output ) -> istream&
    {
        return getline( stream , output , '\t' );
    };

    if( !get_column( output.sequence_name ) )
        throw malformed_snp( "missing first column (chromosome name)" );
    // remove spaces from sequence name:
    auto new_sequence_name_end = remove( output.sequence_name.begin() , output.sequence_name.end() , ' ' );
    output.sequence_name.resize( distance( output.sequence_name.begin() , new_sequence_name_end ) );
    // is this line a comment? if so, skip it
    if( !output.sequence_name.empty() && output.sequence_name.front() == '#' )
        return false;

    if( !get_column( buffer ) )
        throw malformed_snp( "missing second column (position)" );
    try
    {
        unsigned long long_position = stoul( buffer );
        if( long_position > numeric_limits< uint32_t >::max() || long_position == 0 )
            throw out_of_range("");
        output.position = long_position - 1; // position in dbsnp is one-based
    } catch( const out_of_range& exc)
    {
        throw malformed_snp( "value for position (second column) is too large or not positive" );
    } catch( const invalid_argument& exc )
    {
        throw malformed_snp( "value for position (second column) has to be an unsigned integer" );
    }

    if( !get_column( output.snp_id ) )
        throw malformed_snp( "missing third column (snp id)" );

    if( !get_column( buffer ) )
        throw malformed_snp( "missing fourth column (reference)" );
    if( buffer.size() != 1 ) // reference is not a single base, so this is not a snp
        return false;
    optional_base ref_base = to_base( buffer.front() );
    if( !is_base( ref_base ) )
        throw malformed_snp( "invalid base as reference (fourth column): " + buffer );
    output.reference_base = base( ref_base );


    if( !get_column( buffer ) )
        throw malformed_snp( "missing fifth column (alternatives)" );
    output.alternatives.fill( optional_base::N() );
    stringstream buffer_stream( buffer );
    // split column at ',' into each individual alternative
    string alt;
    while( getline( buffer_stream , alt , ',' ) )
    {
        // remove spaces
        alt = string( alt.begin() , remove( alt.begin() , alt.end() , ' ' ) );
        if( alt.size() == 1 )
        {
            // this is a single-base alternative
            
            optional_base alternative = to_base( alt.front() );
            // alternative has to be a valid base
            if( !is_base( alternative ) )
                throw malformed_snp( "invalid alternative (fifth column): " + alternative );
            if( alternative == output.reference_base )
                throw malformed_snp( "the reference base can not be listed as alternative" );

            auto alt_begin = output.alternatives.begin();
            auto alt_end = output.alternatives.end();
            // check if this alternative is already in alternatives
            if( find( alt_begin , alt_end , alternative ) == alt_end )
            {
                // if not, add it
                auto unused_alt = find( alt_begin , alt_end , optional_base::N() );
                assert( unused_alt != alt_end ); // this can not happen regardless of input
                *unused_alt = alternative;
            }
        }
    }

    return true;
}




db_snp load_db_snp( istream& is )
{
    db_snp result;

    string line;
    db_snp_line_info line_info;
    unsigned int line_number = 1;
    while( getline( is , line ) )
    {
        istringstream line_stream( line );
        try
        {
            if( read_snp_line( line_stream , line_info ) )
            {
                db_snp::entry entry;
                // set snp id
                entry.id_start_index = result.id_strings_container.size();
                copy( line_info.snp_id.begin() , line_info.snp_id.end() , back_inserter( result.id_strings_container ) );
                result.id_strings_container.push_back( 0 );
                // simply copy other fields
                entry.position = line_info.position; 
                entry.reference_base = line_info.reference_base;
                entry.alternatives = line_info.alternatives;

                vector< db_snp::entry >& container = result.entries[ move( line_info.sequence_name ) ];
                container.push_back( entry );
            }
        } catch( const malformed_snp& exc )
        {
            cerr << "warning: malformed dbsnp vcf on line " + to_string( line_number ) << endl;
            cerr << "\t" << exc.what() << endl;
        }

        ++line_number;
    }

    // for each sequence, sort all entries according to their position
    for( pair< const string , vector< db_snp::entry > >& entries : result.entries )
    {
        sort( entries.second.begin() , entries.second.end() , []( const db_snp::entry& a , const db_snp::entry& b ) -> bool
        {
            return a.position < b.position;
        } );
    }

    return result;
}


