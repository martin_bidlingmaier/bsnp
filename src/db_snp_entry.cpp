#include "db_snp_entry.hpp"

#include <utility>
#include <cstring>
#include <sstream>

using namespace std;

db_snp_entry::db_snp_entry()
{
    has_long_id = false;
    id.small_string.front() = 0;
}
db_snp_entry::db_snp_entry( const db_snp_entry& entry )
{
    position = entry.position;
    chromosome_index = entry.chromosome_index;
    reference_base = entry.reference_base;
    if( entry.has_long_id )
    {
        has_long_id = true;
        
        size_t id_length = strlen( entry.id.long_string );
        id.long_string = new char[ id_length + 1 ];
        strcpy( id.long_string , entry.id.long_string );
    }
    else
    {
        has_long_id = false;

        id.small_string = entry.id.small_string;
    }
}
db_snp_entry::db_snp_entry( db_snp_entry&& entry )
{
    position = entry.position;
    chromosome_index = entry.chromosome_index;
    reference_base = entry.reference_base;

    has_long_id = entry.has_long_id;
    id = entry.id;
    
    entry.has_long_id = false;
    entry.id.small_string.front() = 0;
}
db_snp_entry::~db_snp_entry()
{
    if( has_long_id )
        delete[] id.long_string;
}

void db_snp_entry::swap( db_snp_entry& entry )
{
    ::swap( position , entry.position );
    ::swap( chromosome_index , entry.chromosome_index );
    ::swap( reference_base , entry.reference_base );
    
    ::swap( has_long_id , entry.has_long_id );
    ::swap( id , entry.id );
}
db_snp_entry& db_snp_entry::operator=( db_snp_entry entry )
{
    swap( entry );
    return *this;
}

const char* db_snp_entry::get_id() const
{
    if( has_long_id )
        return id.long_string;
    else
        return id.small_string.data();
}
void db_snp_entry::set_id( const string& new_id )
{
    if( has_long_id )
        delete[] id.long_string;
    
    else if( new_id.size() < id.small_string.size() )
    {
        has_long_id = false;
        copy( new_id.begin() , new_id.end() , id.small_string.begin() );
        id.small_string[ new_id.size() ] = 0;
    }
    else
    {
        has_long_id = true;
        id.long_string = new char[ new_id.size() + 1 ];
        copy( new_id.begin() , new_id.end() , id.long_string );
        id.long_string[ new_id.size() ] = 0;
    }
}




bool operator<( const db_snp_entry& a , const db_snp_entry& b )
{
    if( a.chromosome_index > b.chromosome_index )
        return false;
    if( a.chromosome_index < b.chromosome_index )
        return true;

    return a.position < b.position;
}

bool read_next_entry( istream& is , db_snp_entry& entry , const map< string , uint8_t > &sequence_ids )
{
    string line;
    while( getline( is , line ) )
    {
        if( !line.empty() && line.front() == '#' )
            continue;

        istringstream line_stream( line );
        
        string sequence_name;
        line_stream >> sequence_name;
        if( !line_stream )
            continue;

        uint32_t position;
        line_stream >> position;
        if( !line_stream )
            continue;

        string id;
        line_stream >> id;
        if( !line_stream )
            continue;
        
        auto sequence_id_it = sequence_ids.find( sequence_name );
        if( sequence_id_it != sequence_ids.end() )
        {
            entry.chromosome_index = sequence_id_it->second;
            entry.position = position;
            if( id == "." )
                entry.set_id( "" );
            else
                entry.set_id ( id );
            break;
        }
    }

    return is;
}

