#ifndef BASE_TYPES_HPP_
#define BASE_TYPES_HPP_

#include <utility>
#include <array>
#include <cassert>


#ifndef NDEBUG
struct constexpr_assertion_failed{};
#endif

template< class T >
constexpr T assert_and_get( bool assertion , T value )
{
#ifdef NDEBUG
    return value;
#else
    return assertion ? value : throw constexpr_assertion_failed();
#endif
}

class base
{
public:
    static constexpr std::array< base , 4 > values()
    {
        return std::array< base , 4 >
        {
            base( 0 ),
            base( 1 ),
            base( 2 ),
            base( 3 )
        };
    }
    static constexpr size_t number()
    {
        return 4;
    }

    static constexpr base A()
    {
        return base( 0 );
    }
    static constexpr base C()
    {
        return base( 1 );
    }
    static constexpr base G()
    {
        return base( 2 );
    }
    static constexpr base T()
    {
        return base( 3 );
    }
    
    constexpr base()
      : value( 4 )
    {}
    constexpr explicit base( size_t val )
      : value( assert_and_get( val < number() , val ) )
    {}
    
    constexpr bool operator==( base b )
    {
        return value == b.value;
    }
    constexpr bool operator!=( base b )
    {
        return !( *this == b );
    }
    
    constexpr operator uint8_t()
    {
        return assert_and_get( value < 4 , value );
    }
private:
    uint8_t value;
};

class optional_base
{
public:
    static constexpr std::array< optional_base , 5 > values()
    {
        return std::array< optional_base , 5 >
        {
            optional_base( 0 ),
            optional_base( 1 ),
            optional_base( 2 ),
            optional_base( 3 ),
            optional_base( 4 )
        };
    }
    static constexpr size_t number()
    {
        return 5;
    }

    static constexpr optional_base A()
    {
        return optional_base( 0 );
    }
    static constexpr optional_base C()
    {
        return optional_base( 1 );
    }
    static constexpr optional_base G()
    {
        return optional_base( 2 );
    }
    static constexpr optional_base T()
    {
        return optional_base( 3 );
    }
    static constexpr optional_base N()
    {
        return optional_base( 4 );
    }


    constexpr optional_base()
      : value( N() )
    {}
    constexpr optional_base( base b )
      : value( b )
    {}
    constexpr explicit optional_base( size_t val )
      : value( assert_and_get( val < number() , val ) )
    {}
    
    constexpr operator uint8_t()
    {
        return value;
    }
private:
    uint8_t value;
};

inline constexpr bool is_base( optional_base b )
{
    return b != optional_base::N();
}

inline optional_base to_base( char c )
{    
    switch( c )
    {
    case 'A':
        return optional_base::A();
    case 'C':
        return optional_base::C();
    case 'G':
        return optional_base::G();
    case 'T':
        return optional_base::T();
    default:
        return optional_base::N();
    }
}

inline char to_char( optional_base b )
{
    switch( b )
    {
    case optional_base::A():
        return 'A';
    case optional_base::C():
        return 'C';
    case optional_base::G():
        return 'G';
    case optional_base::T():
        return 'T';
    case optional_base::N():
        return 'N';
    default:
        assert( false ); // this may not happen
        return 0;
    }
}

class genotype
{
public:
    static constexpr std::array< genotype , 10 > values()
    {
        return std::array< genotype , 10 >
        {
            genotype( 0 ),
            genotype( 1 ),
            genotype( 2 ),
            genotype( 3 ),
            genotype( 4 ),
            genotype( 5 ),
            genotype( 6 ),
            genotype( 7 ),
            genotype( 8 ),
            genotype( 9 ),
        };
    }
    static constexpr size_t number()
    {
        return 10;
    }

    constexpr genotype( base a , base b )
      : genotype_index( a <= b ? 
            get_genotype_index( a , b ) :
            get_genotype_index( b , a ) )
    {}
    constexpr explicit genotype( uint8_t index )
      : genotype_index( assert_and_get( index < 10 , index ) )
    {}

    constexpr operator uint8_t()
    {
        return genotype_index;
    }

    constexpr base a()
    {
        return genotypes[ genotype_index ].first;
    }
    constexpr base b()
    {
        return genotypes[ genotype_index ].second;
    }
private:
    static const constexpr std::pair< base , base > genotypes[ 10 ] =
    {
        std::make_pair( base::A() , base::A() ),
        std::make_pair( base::A() , base::C() ),
        std::make_pair( base::A() , base::G() ),
        std::make_pair( base::A() , base::T() ),

        std::make_pair( base::C() , base::C() ),
        std::make_pair( base::C() , base::G() ),
        std::make_pair( base::C() , base::T() ),

        std::make_pair( base::G() , base::G() ),
        std::make_pair( base::G() , base::T() ),

        std::make_pair( base::T() , base::T() )
    };
    static constexpr uint8_t get_genotype_index( base lower , base higher )
    {
        // math
        return ( lower * ( 7 - lower ) ) / 2 + higher;
    }
    
    uint8_t genotype_index;
};

constexpr bool is_homozygous( genotype g )
{
    return g.a() == g.b();
}
constexpr bool is_heterozygous( genotype g )
{
    return !is_homozygous( g );
}


enum class strand
{
    PLUS,
    MINUS
};

struct base_info
{
    base b;
    strand st;
    uint8_t base_quality;
    uint16_t mapping_quality;
};


#endif

