#ifndef GENOTYPE_PRIOR_PROBABILITIES_HPP_
#define GENOTYPE_PRIOR_PROBABILITIES_HPP_

#include "base_types.hpp"

#include <array>

const std::array< double , genotype::number() >& genotype_prior_probabilities( optional_base reference_base );

#endif

