#ifndef FASTA_FILE_HPP_
#define FASTA_FILE_HPP_

#include "base_types.hpp"

#include <map>
#include <string>
#include <istream>
#include <vector>

class indexed_fasta_file
{
public:
    indexed_fasta_file( std::istream& fasta_file , std::istream& index );
    std::vector< optional_base > load_sequence( const std::string& sequence_name );
    std::vector< std::string > sequences() const;
    const std::map< std::string , std::pair< size_t , size_t > >& get_index() const;
private:
    std::istream& fasta_file;
    std::map< std::string , std::pair< size_t , size_t > > fasta_index;
};

#endif

