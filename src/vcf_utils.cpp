#include "vcf_utils.hpp"

#include <iterator>
#include <algorithm>

using namespace std;
using namespace BamTools;

static const char* common_header =
    "##fileformat=VCFv4.1\n"
    "##FORMAT=<ID=AD,Number=.,Type=Integer,Description=\"Allelic depths for the ref and alt alleles in the order listed\">\n"
    "##FORMAT=<ID=DP,Number=1,Type=Integer,Description=\"Read Depth (only filtered reads used for calling)\">\n"
    "##FORMAT=<ID=GQ,Number=1,Type=Float,Description=\"Genotype Quality calculated as GQ=-10*log_10(P(g_best|D)/P(g_second|D))\">\n"
    "##FORMAT=<ID=GT,Number=1,Type=String,Description=\"Genotype\">\n"
    "##FORMAT=<ID=PL,Number=G,Type=Integer,Description=\"Normalized, Phred-scaled posteriors for genotypes as defined in the VCF specification\">\n"
    "##INFO=<ID=AC,Number=A,Type=Integer,Description=\"Allele count in genotypes, for each ALT allele, in the same order as listed\">\n"
    "##INFO=<ID=AF,Number=A,Type=Float,Description=\"Allele Frequency, for each ALT allele, in the same order as listed\">\n"
    "##INFO=<ID=AN,Number=1,Type=Integer,Description=\"Total number of alleles in called genotypes\">\n"
    "##INFO=<ID=DB,Number=0,Type=Flag,Description=\"dbSNP Membership\">\n"
    "##INFO=<ID=DP,Number=1,Type=Integer,Description=\"Filtered Depth\">\n"
    "##INFO=<ID=MQ,Number=1,Type=Float,Description=\"RMS Mapping Quality\">\n";

void write_vcf_header( ostream& os , const SamHeader& sam_header , const std::string& reference_file_path )
{
    os << common_header;
    
    // range based for doesn't work because 'Begin' and 'End' are capitalized
    for( auto it = sam_header.Sequences.Begin() ; it != sam_header.Sequences.End() ; ++it )
    {
        const SamSequence& sequence = *it;

        os << "##contig=<ID=";
        if( sequence.HasName() )
            os << sequence.Name;
        else
            os << "NA";
        
        os << ",length=";
        if( sequence.HasLength() )
            os << sequence.Length;
        else
            os << "NA";

        os << ",assembly=";
        if( sequence.HasAssemblyID() )
            os << sequence.AssemblyID;
        else
            os << "NA";

        os << "\n";
    }

    os << "##reference=file: " << reference_file_path << "\n";
    os << "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\tImr90" << "\n";
}

