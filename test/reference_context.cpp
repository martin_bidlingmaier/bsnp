#include <fasta_file.hpp>
#include <reference_context.hpp>

#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

// convert range of optional_bases to vector of chars
template< class IT >
string to_chars( IT begin , IT end )
{
    string result;
    for( IT it = begin ; it != end ; ++it )
        result.push_back( to_char( *it ) );
    return result;
}

int main()
{
    string fasta_path = "res/partial-ref-chr1.fa";
    string fasta_index_path = fasta_path + ".fai";
    
    ifstream fasta_file( fasta_path );
    ifstream fasta_index_file( fasta_index_path );

    indexed_fasta_file fasta( fasta_file , fasta_index_file );

    vector< optional_base > sequence = fasta.load_sequence( "1" );
    typedef vector< optional_base >::iterator reference_iterator;
    reference_context< reference_iterator > context( sequence.begin() , sequence.end() );

    context.jump_to_position( 15366 );

    // test (zero based) position 15364 to 15378
    string actual_content = "TGCCCAGGGCACTG";
    auto actual_context_begin = actual_content.begin();
    auto actual_context_end = actual_content.begin() + 5;

    for( int i = 15366 ; i <= 15375 ; ++i )
    {
        auto current_loaded_context = context.get_current_context();
        string loaded_context = to_chars( current_loaded_context.begin() , current_loaded_context.end() );
        string actual_context = string( actual_context_begin , actual_context_end );
        if( loaded_context != actual_context )
        {
            cerr << "loaded reference context is not correct at " << i << endl;
            cerr << "actual context: " << actual_context << endl;
            cerr << "loaded context: " << loaded_context << endl;
            return 1;
        }
        ++actual_context_begin;
        ++actual_context_end;
        context.increment_position();
    }
}

