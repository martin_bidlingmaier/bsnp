#include <snp.hpp>
#include <snp_model.hpp>

#include <iostream>

using namespace std;

int main()
{
    snp_test_result test;
    test.most_likely_genotype = make_pair( BASE::A , BASE::C );

    test.error_probability = 0.1;
    test.mapping_quality = 85.34564;
    
    test.base_specific_depths = { 10 , 13 , 1 , 0 };
    test.sample_depth = 24;

    cout << "one mutation: " << endl;
    snp s( 4 , 385 , BASE::A , test );
    cout << s <<  endl;

    cout << "two mutations, heterozygote" << endl;
    s = snp( 4 , 385 , BASE::G , test );
    cout << s << endl;

    cout << "two mutations, homozygote" << endl;
    test.most_likely_genotype = make_pair( BASE::A , BASE::A );
    s = snp( 4 , 385 , BASE::G , test );
    cout << s << endl;
}

