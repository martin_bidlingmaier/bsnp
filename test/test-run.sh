#!/bin/sh

source misc/prepare-local-run

gdb --args build/debug/bin -q 20 -c 0.98 -d res/partial-dbsnp-chr1.vcf res/test-bam.bam res/partial-ref-chr1.fa output-vcf.vcf 

