#include <api/BamReader.h>
#include <api/BamAlignment.h>
#include <positional_bases.hpp>
#include <algorithm>

using namespace BamTools;
using namespace std;

int main()
{
    BamReader reader;
    if( !reader.Open( "res/test-bam.bam" ) )  
    {  
        cout << "failed to open" << endl;
        return 1;
    }
    if( !reader.OpenIndex( "res/test-bam.bam.bai" ) )
    {
        cout << "failed to load index" << endl;
        return 1;
    }
    
    auto base_filter = []( const base_info& ){ return true; };
    auto bam_filter = []( const BamAlignment& ){ return true; };

    positional_bases bases( reader , ALIGNER::BISMARK , base_filter , bam_filter );
    int32_t reference_id = reader.GetReferenceID( "1" );
    assert( reference_id != -1 );
    
    if( !bases.jump_to_position( reference_id , 9990) )
    {
        cout << "jump_to_position failed" << endl;
        return 2;
    }

    for( int i = 0 ; i < 10 ; ++i )
    {
        cout << "position : " << bases.position() << endl;
        cout << "number of overlaps: " <<  distance( bases.begin() , bases.end() ) << endl;
        cout << "bases: ";
        for( base_info bi : bases )
            cout << to_char( bi.b );
        cout << endl;
        bases.increment_position();
    }

}
 
