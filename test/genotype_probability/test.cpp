#include <api/BamReader.h>
#include <api/BamAlignment.h>
#include "src/genotype_probability.hpp"
#include <algorithm>
#include <utility>

using namespace BamTools;
using namespace std;

pair< genotype , double > most_likely_genotype( const array< double , GENOTYPE_NUMBER >& probabilities )
{
    double max_prob = 0.0;
    size_t max_index = 0;
    for( size_t i = 0 ; i != GENOTYPE_NUMBER ; ++i )
    {
        if( max_prob < probabilities[ i ] )
        {
            max_prob = probabilities[ i ];
            max_index = i;
        }
    }
    return make_pair( genotypes[ max_index ] , max_prob );
} 

vector< base_info > data1 =
{
    { BASE::A , STRAND::MINUS , 0.0 } ,
    { BASE::A , STRAND::MINUS , 0.0 } ,
    { BASE::A , STRAND::MINUS , 0.0 } ,
    { BASE::A , STRAND::MINUS , 0.0 } ,
    { BASE::A , STRAND::MINUS , 0.0 }
};

vector< base_info > data2 =
{
    { BASE::A , STRAND::MINUS , 0.0 } ,
    { BASE::T , STRAND::MINUS , 0.0 } ,
    { BASE::A , STRAND::MINUS , 0.0 } ,
    { BASE::T , STRAND::MINUS , 0.0 } 
};

vector< base_info > data3 =
{
    { BASE::T , STRAND::PLUS , 0.01 } ,
    { BASE::T , STRAND::PLUS , 0.01 } ,
    { BASE::T , STRAND::PLUS , 0.01 } ,
    { BASE::T , STRAND::PLUS , 0.01 } ,
    { BASE::T , STRAND::PLUS , 0.01 } ,
    { BASE::C , STRAND::MINUS , 0.01 } ,
    { BASE::C , STRAND::MINUS , 0.01 } ,
    { BASE::C , STRAND::MINUS , 0.01 } ,
    { BASE::C , STRAND::MINUS , 0.01 } ,
    { BASE::C , STRAND::MINUS , 0.01 }
};


int main()
{
    auto result1 = determine_genotype_probabilities( data1.begin() , data1.end() , 0 , 0.5 , 0 );
    auto likely_genotype1 = most_likely_genotype( result1 );
    if( likely_genotype1.first != make_pair( BASE::A , BASE::A ) && likely_genotype1.second != 1.0 )
        cout << "failed test one" << endl;
    
    auto result2 = determine_genotype_probabilities( data2.begin() , data2.end() , 0 , 0.5 , 0 );
    auto likely_genotype2 = most_likely_genotype( result2 );
    if( likely_genotype2.first != make_pair( BASE::A , BASE::T ) )
        cout << "failed test two" << endl;
    
    auto result3 = determine_genotype_probabilities( data3.begin() , data3.end() , 0 , 1.0 , 0 );
    auto likely_genotype3 = most_likely_genotype( result3 );
    if( likely_genotype3.first != make_pair( BASE::C , BASE::C ) )
    {
        for( int i = 0 ; i < 10 ; ++i )
            cout << result3[ i ] << endl;
        cout << "failed test three" << " " << likely_genotype3.first.first << "   " << likely_genotype3.first.second << endl;
    }
}

