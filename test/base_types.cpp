#include <base_types.hpp>
#include <array>
#include <iostream>
#include <algorithm>

using namespace std;

int main()
{
    genotype genotypes[] = 
    {
        genotype( base::A() , base::A() ),
        genotype( base::A() , base::C() ),
        genotype( base::A() , base::G() ),
        genotype( base::A() , base::T() ),

        genotype( base::C() , base::C() ),
        genotype( base::C() , base::G() ),
        genotype( base::C() , base::T() ),

        genotype( base::G() , base::G() ),
        genotype( base::T() , base::G() ),

        genotype( base::T() , base::T() )
    };
    
    sort( begin( genotypes ) , end( genotypes ) );
    
    for( size_t i = 0 ; i != 10 ; ++i )
        assert( i == genotypes[ i ] );
    
    cout << sizeof( base ) << endl;
}

