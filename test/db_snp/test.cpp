#include <db_snp.hpp>

#include <iostream>
#include <fstream>

using namespace std;

int main()
{
    ifstream db_snp_file( "db_snp.vcf" );
    if( db_snp_file.fail() )
    {
        cerr << "unable to open db snp file" << endl;
        return 1;
    }
    db_snp db = load_db_snp( db_snp_file );
    for( const auto& sequence : db.entries )
    {
        cout << sequence.first << "\t" << sequence.second.size() << endl;
        for( db_snp::entry e : sequence.second )
            cout << "\t" << db.id_strings_container.data() + e.id_start_index << endl;
    }
}

