#include <fasta_file.hpp>

#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

// convert range of optional_bases to vector of chars
template< class IT >
string to_chars( IT begin , IT end )
{
    string result;
    for( IT it = begin ; it != end ; ++it )
        result.push_back( to_char( *it ) );
    return result;
}

int main()
{
    string fasta_path = "res/partial-ref-chr1.fa";
    string fasta_index_path = fasta_path + ".fai";
    
    ifstream fasta_file( fasta_path );
    ifstream fasta_index_file( fasta_index_path );

    indexed_fasta_file fasta( fasta_file , fasta_index_file );
    const auto& index = fasta.get_index();

    if( index.find( "1" ) == index.end() )
    {
        cerr << "failed to read chromosome \"1\" from index" << endl;
        return 1;
    }
    
    vector< optional_base > sequence = fasta.load_sequence( "1" );
    if( sequence.size() != 299940 )
    {
        cerr << "loaded sequence does not have correct length" << endl;
        return 1;
    }
    
    // test (zero based) position 15364 to 15378
    string actual_content = "TGCCCAGGGCACTG";
    string loaded_content = to_chars( sequence.begin() + 15364 , sequence.begin() + 15378 );
    if( actual_content != loaded_content )
    {
        cerr << "loaded sequence is not correct in [15364, 15378)" << endl;
        return 1;
    }
}


