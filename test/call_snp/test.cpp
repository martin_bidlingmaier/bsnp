#include <load_fasta.hpp>
#include <call_snp.hpp>
#include <positional_bases.hpp>

#include <api/BamReader.h>
#include <api/BamAlignment.h>

#include <cassert>
#include <fstream>
#include <string>
#include <map>

using namespace BamTools;
using namespace std;

void test_result_presenter( size_t position , base_t ref_base , const snp_test_result& test )
{
    cout << "position: " << position << endl;
    cout << "   reference: " << base_to_char( ref_base ) << endl;
    cout << "   genotype: " << base_to_char( test.most_likely_genotype.first ) << " " << base_to_char( test.most_likely_genotype.second ) << endl;
    cout << "   error probability " << test.error_probability << endl;
    cout << "   mapping quality " << test.mapping_quality << endl;
}
    
int main()
{
    BamReader bam_file;
    if( !bam_file.Open( "test-bam.bam" ) )  
    {  
        std::cout << "failed to open" << std::endl;
        return 1;
    }
    if( !bam_file.OpenIndex( "test-bam.bam.bai" ) )
    {
        std::cout << "failed to load index" << std::endl;
        return 1;
    }
    
    ifstream fasta_file( "chr22.fa" );
    assert( fasta_file.good() );
    map< string , string > reference_sequences = load_fasta( fasta_file );
    auto chr22_sequence_it = reference_sequences.find( "chr22" );
    assert( chr22_sequence_it != reference_sequences.end() );
    reference_context context( chr22_sequence_it->second.begin() , chr22_sequence_it->second.end() );
    
    snp_config config;
    config.alpha = 0.01;
    config.gamma = 0.0;
    config.minimum_mapping_quality = 17;
    config.minimum_base_quality = 17;
    config.aligner = ALIGNER::BISMARK;
    
    genome_region region;
    region.sequence_id = bam_file.GetReferenceID( "1" );
    region.start_position = 9990;
    region.length = 15;

    call_snp_on_region( bam_file , context , config , region , test_result_presenter );
}

