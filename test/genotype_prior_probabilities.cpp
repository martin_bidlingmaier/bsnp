#include <genotype_prior_probabilities.hpp>
#include <iostream>

using namespace std;

int main()
{
    for( optional_base ref_base : optional_base::values() )
    {
        cout << "base: " << to_char( ref_base ) << endl;
        const auto& priors = genotype_prior_probabilities( ref_base );
        double sum = 0.0;
        for( genotype g : genotype::values() )
        {
            cout << "(" << to_char( g.a() ) << "," << to_char( g.b() ) << "): " << priors[ g ] << endl;
            sum += priors[ g ];
        }
        cout << "sum: " << sum << endl;
        cout << endl;
    }
}
