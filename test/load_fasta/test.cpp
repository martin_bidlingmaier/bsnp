#include <load_fasta.hpp>

#include <fstream>
#include <iostream>
#include <cassert>

using namespace std;

int main()
{
    ifstream fasta_file( "test.fa" );
    assert( fasta_file.good() );
    ifstream fasta_index_file( "test.fa.fai" );
    assert( fasta_index_file.good() );
    
    indexed_fasta_file fasta( fasta_file , fasta_index_file );
    vector< string > sequences = fasta.sequences();
    
    ofstream os( "out.fa" );
    os << ">2" << endl;
    vector< char > data = fasta.load_sequence( "2" );
    auto it = data.begin();
    while( distance( it , data.end() ) >= 9 )
    {
        os << string( it , it + 9 ) << endl;
        it += 9;
    }
    os << string( it , data.end() ) << endl;
        
    for( auto& s : sequences )
        cout << s << endl;
}

