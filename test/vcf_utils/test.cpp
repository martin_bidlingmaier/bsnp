#include "vcf_utils.hpp"

#include <fstream>
#include <iostream>
#include <cassert>
#include <iterator>

using namespace std;

int main()
{
    map< string , size_t > sequence_ids;
    sequence_ids[ "chrM" ] = 0;

    ifstream db_snp_file( "dbsnp.vcf" );
    db_snp_entry entry;
    while( read_next_entry( db_snp_file , entry , sequence_ids ) )
    {
        cout << "position: " << entry.position;
        cout << "   id: " << entry.get_id() << endl;
    }
}


